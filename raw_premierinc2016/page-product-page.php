<?php 

/**
 * Template Name: Product Page
 *
 */

get_header();?>
    <nav id="product_sheet_filter" class="product_sheet_filter">
        <?php product_sheet_nav(); ?>
    </nav>
    <main role="main">
        <!-- section -->

        <section>
            <div class="product_sheet_content_holder" id="product-sheet-content-holder" aria-live="polite">
                <div class="product_sheet_content_loader" id="product-sheet-content-loader">
                    <div class="loader"></div>
                </div>
            </div>
        </section>
        <section>

            <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                <!-- article -->
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <?php the_content(); ?>

                        <br class="clear">

                        <?php edit_post_link(); ?>

                </article>
                <!-- /article -->

                <?php endwhile; ?>

                    <?php else: ?>

                        <!-- article -->
                        <article>

                            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

                        </article>
                        <!-- /article -->

                        <?php endif; ?>

        </section>
        <!-- /section -->
    </main>

    <?php get_footer(); ?>