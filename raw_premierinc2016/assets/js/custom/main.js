window.onpageshow = function (event) {
    if (event.persisted) {
        window.location.reload()
    } //Prevents safari from caching pages. Associated with the page fade in / out animations 
};
$(window).load(function () {
    
    if ($('html').hasClass('js')) { //Modernizer - if we are using javascript
        var wrapper = $('.wrapper');
        TweenLite.to(wrapper, .2, {
            opacity: '1'
        }); //Fade in the page wrapper
    }
});
$(window).load(function () {
    var navheight = $('.header').outerHeight(),
        windowHeight = window.innerHeight,
        sliderHeight = windowHeight - navheight;
    $('.nav .sub-menu').css({
        'top': navheight + 'px'
    }); //On page load, adjust the position of the sub menus in the nav menu. Don't know if this is the best way of doing this. Maybe circle back on this before things go live.
    $('.product_sheet_filter').css({
        'padding-top': navheight + 'px'
    }); //On page load, adjust the position of the top padding on the filter navigation. pushes it out of the way of the main nav
    $('.search_container').css({
        top: navheight + 'px'
    }); //Line up the search container with the filter nav
    var filterHeight = $('.product_sheet_filter').outerHeight();
    
    $('.product_sheet_filter > ul > li >.sub-menu').css({
        top: navheight + 40 + 'px'
    }); //Set the top of the sub menus to line up with the product sheet filter navigation so it lines up in the drawer
});

$(document).ready(function () {

    /*-------------------------*\
        Filter Navigation
    \*-------------------------*/

    //Ajax Search Function

    function productSheetAjaxSearch(e) {
        if (!$('#filter-sub-menu-drawer').hasClass('filter_drawer_open_1')) { //If the drawer is not in position 1
            subNavDrawerPosOne(); //Put the sub nav drawer in position 1
        } else { //If it is either clsoed or in position 2
            $('.filter_sub_nav_open > ul').fadeOut(250, function () { //Fade out the first ul child of the active navigation item
                $('.filter_sub_nav_open').removeClass('filter_sub_nav_open'); //Take away the active navigation class
            });
        }
        $('.loader_holder').fadeIn(250); //Fade in the loading animation
        e.preventDefault(); //Keep the browser from following the search links if the "search" button is clicked
        var search_val = $('.search-input').val(); //Set the search value with the contents of the form
        if (search_val !== null || search_val !== '') { //If the search value is not null or empty
            $.ajax({
                type: "POST",
                url: myAjax.ajaxurl,
                data: {
                    action: 'product_sheet_filter_search',
                    query: search_val
                }, //Set up the ajax call
                success: function (data) {
                    $('#filter-search-results-holder').html(data); //Use the results of the ajax call to fill the search results
                    
                    setTimeout(function () {
                        $('.loader_holder').fadeOut(250); //Fade Out the loading animation
                    }, 500);
                    setTimeout(function () {
                        $('#filter-search-results-holder').fadeIn(250); //Fade in the search results
                        $('#filter-search-results-holder').addClass('filter_search_results_display'); //Give the search results the displayed class
                    }, 750);
                }
            });
        }
    }
    
    //Ajax Post Retrieval

    function productSheetGetAjax(e) {
        
        var theUrl = e.href, //Get the href attribute of the clicked element. We are retrieving the post by URL becaues it is a custom post type. 
            productSheetLoader = document.getElementById('product-sheet-content-loader'); //Sets a variable for the loader animation. Saves us time with the GSAL
        $('#product-sheet-content-loader').css({
            display: 'block'
        }); //Set the loader to be visible
        setTimeout(function () {
            TweenLite.to(productSheetLoader, .25, {
                opacity: 1
            }); //Wait 5ms and fade in the loader
        }, 5);
        $.ajax({
            type: "POST",
            url: myAjax.ajaxurl,
            data: {
                action: 'product_sheet_get',
                query: theUrl
            }, //Set up the Ajax call for the posts
            success: function (data) {
                
                $('.product_sheet_content_holder').attr('aria-live', 'false'); //Accessibility - Remove live status from the block holding the content for screen readers

                setTimeout(function () { //Wait 500ms for loader animation to do it's job
                    $('#product-sheet-content-holder').html(data); //Set the contents of the product sheet holder to the returned data from the Ajax call
                    
                    $('.product_sheet_content_holder').attr('aria-live', 'polite'); //A11y - Set the aria live status to polite to read after it has appeared on screen

                    history.pushState('data', '', theUrl); //Update the URL in the browser. This gives the user the ability to navigate backwards and assigns history to the Ajax posts. Pretty cool eh?
                    
                    TweenLite.to('#product-sheet-content-loader', .25, {
                        opacity: 0
                    }); //Fade out the loader
                    
                    $('#product-sheet-content-holder').find('h2').first().focus(); //Accessibility - Set the focus to the first h2 in the content
                    setTimeout(function () {
                        $('#product-sheet-content-loader').css({
                            display: 'none'
                        }); //Set the display of the loader to none
                    }, 250);
                }, 500);
            }
        });
    }

    $('.menu-item-object-product_sheet > a').on('click', function () {
        productSheetGetAjax(this);
        return false;
    });
    
    $('.search_result_page_link').on('click', function() {
        alert('wtf');
//         productSheetGetAjax(this);
         return false;
    });

    var timer;
    $('.search-input').on('keyup', function (e) {
        clearTimeout(timer);
        var ms = 500; // milliseconds
        timer = setTimeout(function () {
            if ($('.search-input').val().length > 2) {
                $('#filter-search-results-holder').fadeOut(250, function () {
                    productSheetAjaxSearch(e);
                });
            } else if ($('.search-input').val().length === 0 && !$('.product_sheet_filter > ul > li').hasClass('filter_sub_nav_open')) {
                $('#filter-search-results-holder').fadeOut(250, function () {
                    TweenLite.to('#filter-sub-menu-drawer', .25, {
                        height: 0
                    });
                    $('#filter-sub-menu-drawer').removeClass('filter_drawer_open_1');
                    $('.loader_holder').fadeOut(250);

                    $('#filter-search-results-holder').removeClass('filter_search_results_display');
                });

            }
        }, ms);
    });

    $('.search-submit').click(function (e) {
        productSheetAjaxSearch(e);
    });

    function subNavDrawerPosOne(toggle, handledId) {
        var subNavDrawer = document.getElementById('filter-sub-menu-drawer'),
            subSubNavDrawer = document.getElementById('filter-sub-sub-menu-drawer'),
            subNavFlag = document.getElementById('sub-menu-flag-holder');
        if ($(subNavDrawer).hasClass('filter_drawer_open_2')) {

            TweenLite.to(subNavFlag, .1, {
                transform: 'translateX(-200px)'
            });

            TweenLite.to(subSubNavDrawer, .25, {
                height: '0px'
            });
            $(subNavDrawer).removeClass('filter_drawer_open_2');
            $('.filter_sub_nav_open > ul > li > ul').fadeOut(250);
    
            TweenLite.to(subNavDrawer, .25, {
                height: '100px'
            });

            $('.filter_sub_nav_open > ul').fadeOut(250, function () {
                $('.filter_sub_nav_open').removeClass('filter_sub_nav_open');
                $('.product_sheet_filter .sub-menu > li > a').css({
                    display: 'inline'
                });
            });
            $(subNavDrawer).addClass('filter_drawer_open_1');
        } else if ($(subNavDrawer).hasClass('filter_drawer_open_1')) {
            if (toggle = false) {
                //do Nothing
            } else if (toggle = true) {
                $(handledId).removeClass('filter_sub_nav_open'); //Remove the open id from the top level
                setTimeout(function() {
                    TweenLite.to("#filter-sub-menu-drawer", .25, {
                    height: '0px'
                });
                }, 250);
                $('#filter-sub-menu-drawer').removeClass('filter_drawer_open_1'); //Remove the open class from the drawer
            }
        } else {

            TweenLite.to(subNavDrawer, .25, {
                height: '100px'
            });

            $(subNavDrawer).addClass('filter_drawer_open_1');
        }
    }

    /*--------------*\
        Opens and closes the sub nav drawer.
        If you are in initial state, it opens the first sub list of the item you clicked in a sub nav drawer below the first level.
        If the item you clicked is already active, it closes the sub nav drawer.
        If the item you clicked is already active and the sub sub nav drawer is open as well as the sub nav drawer, it closes both drawers.
    \*--------------*/

    function filterNavExpand(elem) {

        //Define variables

        var itemId = elem.parentNode.id, //Get the clicked links parent ID
            handledId = '#' + itemId; //Allow the parent ID to easily be used in jquery functions

        if ($(handledId).hasClass('filter_sub_nav_open')) { //If the menu item is clicked and is already active

            $(handledId + ' > ul').fadeOut(250, function () {
                TweenLite.to('#filter-sub-menu-drawer', .25, {
                    height: '0px'
                });
            }); //Fade out the first sub nav level

            $('.filter_sub_nav_open > li > a').focus();

            subNavDrawerPosOne(true, handledId);

        } else if ($('#filter-sub-menu-drawer').hasClass('filter_drawer_open_2')) { //If the sub drawer is in position 2 and the clicked item is not active
            subNavDrawerPosOne(true, handledId);
            setTimeout(function () { //Wait 550ms until after the drawer is open

                $(handledId + ' > ul').fadeIn(250); //Fade in the clicked item's sub list

                $(handledId).addClass('filter_sub_nav_open'); //Give the clicked item the active class

                $(handledId + ' > ul').find('li > a').first().focus();

            }, 550);

        } else { //If no sub nav is open, or if the sub nav drawer is in position 1
            if ($('#filter-search-results-holder').hasClass('filter_search_results_display')) {
                $('#filter-search-results-holder').fadeOut(250, function () {
                    $('#filter-search-results-holder').removeClass('filter_search_results_display');
                });
            }
            $('.filter_sub_nav_open > ul').fadeOut(250, function () {

                $('.filter_sub_nav_open').removeClass('filter_sub_nav_open'); //Take away active class from old active item

            });

            setTimeout(function () { //Wait for active class to be reomved

                TweenLite.to('#filter-sub-menu-drawer', .25, {
                    height: '100px'
                });

                $('#filter-sub-menu-drawer').addClass('filter_drawer_open_1'); //Give the drawer the position 1 class

                setTimeout(function () { //Wait until the drawer is open

                    $(handledId + ' > ul').fadeIn(250); //Fade in the clicked item's first sub list

                    $(handledId).addClass('filter_sub_nav_open'); //Give the clicked item the active class

                    $(handledId + ' > ul').find('li > a').first().focus();

                }, 250);

            }, 250);
        }
    }

    /*--------------*\
        Opens the sub sub nav drawer of the clicked item.
        If only the sub nav drawer is open, the sub sub nav drawer of the clicked item is opened.
    \*--------------*/

    function filterSubNavExpand(elem) {
        //Define Variables
        var subItemParentId = elem.parentNode.id, //Get the clicked item's parent ID
            subItemId = elem.id, //Get the clicked item's ID
            subItemHandled = '#' + subItemId, //Allow variable to easily be used in a jQuery statement
            subItemParentHandled = '#' + subItemParentId; //Allow variable to easily be used in a jQuery statement

        var newContent = elem.innerHTML; //Get the inner content of the clicked item.

        $('#sub-menu-flag').html(newContent); //Assign the Sub Menu Flag the content of the clicked item

        $('.filter_sub_nav_open > ul > li > a').fadeOut(250); //Fade out the sub menu list items



        $('#filter-sub-menu-drawer').removeClass('filter_drawer_open_1'); //Remove position 1 class
        $('#filter-sub-menu-drawer').addClass('filter_drawer_open_2'); //Add position 2 class
        setTimeout(function () { //Wait 250ms for sub nav drawer to get to position 2

            TweenLite.to('#sub-menu-flag-holder', .1, {
                transform: 'translateX(0px)'
            });

            TweenLite.to('#filter-sub-menu-drawer', .25, {
                height: '40px'
            });

            TweenLite.to('#filter-sub-sub-menu-drawer', .25, {
                height: '100px'
            });

            $(subItemParentHandled + ' > ul').fadeIn(250); //Fade in the clicked item's sub list

            $(subItemParentHandled + ' > ul').find('li > a').first().focus();

        }, 250);

    }

    /*--------------*\
        Closes the sub sub nav drawer when the sub nav flag close button is clicked.
    \*--------------*/

    function filterFlagClick() {

        $('#filter-sub-menu-drawer').removeClass('filter_drawer_open_2'); //Remove position 2 class

        $('#filter-sub-menu-drawer').addClass('filter_drawer_open_1'); //Add position 1 class

        TweenLite.to('#sub-menu-flag-holder', .1, {
            transform: 'translateX(-200px)'
        }); //Take away the sub nav flag

        $('.filter_sub_nav_open > ul > li > ul').fadeOut(250); //Fade out the sub sub nav list

        setTimeout(function () { //Wait 250ms until the sub nav list has faded out and the sub sub drawer has closed

            TweenLite.to('#filter-sub-sub-menu-drawer', .25, {
                height: '0px'
            }); //Close the sub sub drawer

            $('.filter_sub_nav_open > ul > li > a').fadeIn(250); //Fade in the sub nav list items

            TweenLite.to('#filter-sub-menu-drawer', .25, {
                height: '100px'
            }); //Bring the sub nav drawer to position 1 height

        }, 250);
        $('.product_sheet_filter').find('a').first().focus();
    }

    var filterNavHeight = $('#product_sheet_filter').outerHeight();

    $('#sub-menu-flag-close').on('click', function () {

        filterFlagClick();

    });

    $('.product_sheet_filter > ul > .menu-item-object-category > a').click(function () {
        filterNavExpand(this);
        return false;

    });

    $('.product_sheet_filter > ul > .menu-item-object-category > .sub-menu > li.menu-item-object-category > a').click(function () {
        filterSubNavExpand(this);
        return false;

    });

    /*-------------------------*\
        Main Navigation
    \*-------------------------*/

    function mainNavSubExpand(elem) {

        /*-----------*\
        
            note about variable naming conventions: 
            Javascript variables get camelCase
            CSS class names get snake_case
            CSS Id names get hyphen-case
            Some Wordpress classes violate these rules, but we will let it slide for now.
            
        \*-----------*/

        var itemId = elem.parentNode.id, //get the parent of the item that was clicked
            handledId = '#' + itemId, //add a # symbol to the beginning of the variable. Makes it easier to use the variable with jQuery expressions
            subNavItem = $(handledId + ' ul');
        if ($(handledId).hasClass('sub_nav_open')) { //If the clicked item has the class 'sub_nav_open', hide the sub menu and close the drawer
            TweenLite.to(subNavItem, .25, {
                height: "0px"
            });
            setTimeout(function () { //Wait 250 milleseconds to hide the sub menu until the drawer has closed. We define the amount of time with the 250 variable. 
                $(handledId + ' ul').css({ //Select the first unordered list child element of the clicked item.
                    display: 'none'
                }); //hide the sub menu by setting display to none. 
                $(handledId).removeClass('sub_nav_open');
            }, 250);
        } else { //If the drawer is closed. 
            $(handledId + ' ul').css({ //Select the clicked item's first child unordered list
                display: 'block'
            }); //show the sub menu by setting display to block. The item is still hidden becasue it's CSS height is set to 0px
            setTimeout(function () { //wait 100 milleseconds to slide down the unordered list. 
                TweenLite.to(subNavItem, .25, {
                    height: "100px"
                });
                $(handledId + ' ul').find('a').first().focus();
            }, 100);
            $(handledId).addClass('sub_nav_open'); //give the clicked item's parent the class "sub_nav_open" so that we can easily tell what if it is an active item next time it gets clicked.
        }
    }

    $('.nav .menu-item-has-children > a').click(function () {
        mainNavSubExpand(this);
        return false;
    });

    /*-------------------------*\
        Hero Slider Heights
    \*-------------------------*/
    $(".pinc_hero_slider").owlCarousel({
        items: 1,
        nav: true,
        loop: true,
        mouseDrag: false,
        navSpeed: 600,
        fallbackEasing: 'ease-out',
        navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>']
    });
    var navheight = $('.header').outerHeight(),
        windowHeight = window.innerHeight,
        sliderHeight = windowHeight - navheight;
    $('.pinc_hero_slider .item').css({
        'height': windowHeight + 'px',
        'padding-top': (navheight + 20) + 'px'
    });

    /*-------------------------*\
        Member Logos
    \*-------------------------*/
    $(".member_logos").owlCarousel({
        items: 4,
        nav: true,
        loop: true,
        mouseDrag: false,
        navSpeed: 600,
        fallbackEasing: 'ease-out',
        navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>']
    });
    /*-------------------------*\
        Sub Nav - Located in Functions.php
    \*-------------------------*/

});