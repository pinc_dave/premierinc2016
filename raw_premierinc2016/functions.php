<?php
/*
 *  Author: David Olson | @davidgolson
 *  URL: premierinc.com | @premierHA
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

function product_sheet_nav() {
    echo '<div id="sub-menu-flag-holder" class="sub_menu_flag_holder">';
    echo '<span id="sub-menu-flag-close" class="sub_menu_flag_close" role="button" tabindex="0">+</span>';
    echo '<span id="sub-menu-flag" class="sub_menu_flag"></span>';
    echo '</div>';
    
    wp_nav_menu(
        array(
            'theme_location'    =>  'product-sheet-menu',
            'menu'              =>  '',
            'container'         =>  'div',
            'container_class'   =>  'menu-{menu slug}-container',
            'menu_class'        =>  'menu',
            'menu_id'           =>  '',
            'echo'              =>  true,
            'fallback_cb'       =>  'wp_page_menu',
            'before'            =>  '',
            'after'             =>  '',
            'link_before'       =>  '',
            'link_after'        =>  '',
            'items_wrap'        =>  '<ul>%3$s</ul>',
            'depth'             =>  4,
            'walker'            =>  ''
        )
    );
    
    echo '<div id="search-container" class="search_container">';
//    get_search_form();
    echo '<!-- search -->';
    echo '<form class="search" method="get" action="'. esc_url( home_url( '/' ) ) .'" role="search">';
    echo '<input class="search-input" type="search" name="s" placeholder="Begin typing.">';
    echo '<button class="search-submit" type="submit" role="button">Search</button>';
    echo '</form>';
    echo '<!-- /search -->';
    echo '</div>';
    
    echo '<div class="filter_search_results_holder" id="filter-search-results-holder"></div>';
    
    echo '<div class="filter_sub_menu_drawer" id="filter-sub-menu-drawer"></div>';
    echo '<div class="filter_sub_sub_menu_drawer" id="filter-sub-sub-menu-drawer"></div>';
    echo '<div class="loader_holder">';
    echo '<div class="loader"></div>';
    echo '</div>';
    ?>
    <script>
        window.onpopstate = function(event) {
            var currentUrl = window.location.href;
            window.location.href = currentUrl;
        }
    </script>
    <?php
}

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

//    	wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
//        wp_enqueue_script('conditionizr'); // Enqueue it!
//
//        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
//        wp_enqueue_script('modernizr'); // Enqueue it!
        
        wp_register_script('vendor_scripts', get_template_directory_uri() . '/assets/js/vendors.min.js'); // Custom scripts
        wp_enqueue_script('vendor_scripts'); // Enqueue it!
        
        wp_register_script('custom_scripts', get_template_directory_uri() . '/assets/js/custom.min.js'); // Custom scripts
        wp_enqueue_script('custom_scripts'); // Enqueue it!
        wp_localize_script('custom_scripts', 'myAjax', array('ajaxurl' => admin_url('admin-ajax.php')));
        wp_register_script('greensock_css', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/plugins/CSSPlugin.min.js');
        wp_enqueue_script('greensock_css');
        wp_register_script('greensock_ease', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/easing/EasePack.min.js');
        wp_enqueue_script('greensock_ease');
        wp_register_script('greensock_core', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenLite.min.js');
        wp_enqueue_script('greensock_core');
    }
}

// Load HTML5 Blank conditional scripts
//function html5blank_conditional_scripts()
//{
//    if (is_page('pagenamehere')) {
//        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
//        wp_enqueue_script('scriptname'); // Enqueue it!
//    }
//}

// Load HTML5 Blank styles
function html5blank_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/assets/css/normalize.min.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!
    
    wp_register_style('vendorcss', get_template_directory_uri() . '/assets/css/vendors.min.css', array(), '1.0', 'all');
    wp_enqueue_style('vendorcss'); // Enqueue it!

    wp_register_style('html5blank', get_template_directory_uri() . '/style.min.css', array(), '1.0', 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank'), // Extra Navigation if needed (duplicate as many as you need!)
        'product-sheet-menu' => __('Product Sheet Menu', 'html5blank')
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?>
        <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-
            <?php comment_ID() ?>">
                <?php if ( 'div' != $args['style'] ) : ?>
                    <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
                        <?php endif; ?>
                            <div class="comment-author vcard">
                                <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
                                    <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
                            </div>
                            <?php if ($comment->comment_approved == '0') : ?>
                                <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
                                <br />
                                <?php endif; ?>

                                    <div class="comment-meta commentmetadata">
                                        <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
                                            <?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?>
                                        </a>
                                        <?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
                                    </div>

                                    <?php comment_text() ?>

                                        <div class="reply">
                                            <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                                        </div>
                                        <?php if ( 'div' != $args['style'] ) : ?>
                    </div>
                    <?php endif; ?>
                        <?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head

add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination
add_action('init', 'create_post_homepage_sliders'); //Add custom post type - Homepage Sliders
add_action('init', 'create_post_member_logos'); //Add custom post type - Member Logos
add_action('init', 'create_post_featured_sliders'); //Add custom post type - Featured Content Sliders
add_action('init', 'create_post_product_sheet'); //Add custom post type - Product Sheets
add_action('admin_init', 'admin_init'); //Register Custom Meta Boxes for Custom Post Types
add_action('admin_enqueue_scripts', 'wpse_80236_Colorpicker'); //Register Colorpicker
add_action('save_post', 'save_details'); //Saves Custom Metaboxes
add_action('save_post', 'save_radio_box'); //Saves Custom Meta Boxes - Radio Edition

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.
add_shortcode('display_homepage_heros', 'display_homepage_heros');
add_shortcode('display_sub_nav', 'display_sub_nav');
add_shortcode('display_member_logos', 'display_member_logos');

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]


/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

function display_homepage_heros() {
    $args = array(
            'post_type' => 'homepage_sliders',
            'post_status' => 'publish'
        );
    $string = '';
    $query = new WP_Query( $args );
    
    echo '<section>';
    
    echo '<div class="pinc_hero_slider">';
    while ($query->have_posts()) : $query->the_post();
        $meta = get_post_meta(get_the_ID(), '', false);
        echo '<div class="item slider_'.get_the_ID().'" style="background-image: url(\'';
        echo the_post_thumbnail_url('full');
        echo '\')">';
        echo '<h2>'.$meta['slider_heading'][0].'</h2>';
        echo '<p>'.$meta['slider_subheading'][0].'</p>';
        echo '</div>';
    endwhile;
    echo '</div>';
    echo '</section>';
    wp_reset_query();
}

function display_sub_nav() {
    if (is_front_page()){
        $the_Id = get_option( 'page_on_front' );
    } else {
        $the_Id = get_the_ID();
    }
    ?>
    <section id="sub_nav_container">
        <nav id="sub_nav" class="sub_nav">
            <ul>
                <?php
                    $child_args = array(
                        'post_parent' => $the_Id,
                        'post_type' => 'page'
                    );
                    
                    $page_children = get_children($child_args);
                    $counted = count($page_children);
                    foreach ($page_children as $child) {
                        ?>
                        <li class="<?php
                            if ($counted < 4){
                                echo 'col_'.$counted;
                            } else {
                                echo 'col_4';
                            }
                        ?>">
                            <?php
                            echo '<a id="tab_'.$child->ID.'" title="'.$child->post_title.'" href="#'.$child->post_name.'" class="sub_nav_links">';
                            echo '<span class="icon"><i class="icon-shocked"></i></span>';
                            echo '<span>'.$child->post_title.'</span>';
                            echo '</a>';
                            ?>
                        </li>
                        <?
                    }
                ?>
            </ul>
        </nav>
        <div class="clear"></div>
        <div class="sub_nav_drawer closed" id="sub_nav_drawer">
            <?php
                foreach($page_children as $child_2) {
                    echo '<ul id="sub_list_'.$child_2->ID.'" class="sub_nav_list">';
                    $child_args_2 = array(
                        'post_parent' => $child_2->ID,
                        'post_type' => 'page'
                    );
                    $child_children = get_children($child_args_2);
                    $child_count = count($child_children);
                    foreach ($child_children as $child_child) {
                        echo '<li class="col_'.$child_count.'">';
                        echo $child_child->post_title;
                        echo '</li>';
                    }
                    echo '</ul>';
                }
            ?>
        </div>
    </section>
    <script>
    $(document).ready(function () {
        var changeClass = function (r, className1, className2) {
            var regex = new RegExp("(?:^|\\s+)" + className1 + "(?:\\s+|$)");
            if (regex.test(r.className)) {
                r.className = r.className.replace(regex, ' ' + className2 + ' ');
            } else {
                r.className = r.className.replace(new RegExp("(?:^|\\s+)" + className2 + "(?:\\s+|$)"), ' ' + className1 + ' ');
            }
            return r.className;
        };

        var menuElements = document.getElementById('sub_nav');

        document.getElementById('sub_nav').insertAdjacentHTML('afterBegin', '<button type="button" id="sub_nav_toggle" class="sub_nav_toggle" aria-hidden="true"><i aria-hidden="true" class="icon-circle-down"> </i> Menu</button>');

        document.getElementById('sub_nav_toggle').onclick = function () {
            changeClass(this, 'sub_nav_toggle active', 'sub_nav_toggle');
        };

        document.onclick = function (e) {
            var mobileButton = document.getElementById('sub_nav_toggle'),
                buttonStyle = mobileButton.currentStyle ? mobileButton.currentStyle.display : getComputedStyle(mobileButton, null).display;

            if (buttonStyle === 'block' && e.target !== mobileButton && new RegExp(' ' + 'active' + ' ').test(' ' + mobileButton.className + ' ')) {
                changeClass(mobileButton, 'sub_nav_toggle active', 'sub_nav_toggle');
            }
        };

        $('.sub_nav_links').click(function () {
            drawerExpand(this);
            return false;
        });

        function drawerExpand(elem) {
            var buttonId = elem.id,
                index = buttonId.lastIndexOf("_"),
                result = buttonId.substr(index + 1),
                subListId = 'sub_list_' + result,
                drawer = document.getElementById('sub_nav_drawer');
            if ($(drawer).hasClass('closed') && !$(elem).hasClass('active')) {
                $(drawer).removeClass('closed');
                $(drawer).addClass('open');
                $(elem).addClass('active');
                TweenLite.to(drawer, .25, {height: '200px'});
                $('#' + subListId).fadeIn(500);
                $('#' + subListId).addClass('sub_list_active')
            } else if ($(drawer).hasClass('open') && $(elem).hasClass('active')) {
                $(drawer).removeClass('open');
                $(drawer).addClass('closed');
                TweenLite.to(drawer, .25, {height: '0px'});
                $(elem).removeClass('active');
                $('#' + subListId).fadeOut(500);
                document.activeElement.blur();
            } else if ($(drawer).hasClass('open') && !$(elem).hasClass('active')) {
                $('.active').removeClass('active');
                setTimeout(function () {
                    $(elem).addClass('active');
                    $('.sub_list_active').removeClass('sub_list_active');
                }, 1);
                $('.sub_nav_list').fadeOut(250);

                setTimeout(function () {
                    $('#' + subListId).addClass('active');
                    $('#' + subListId).fadeIn(250);
                }, 250);

            }
        }
    });
</script>
    <?php
    wp_reset_query();
}

function display_member_logos() {
    $args = array(
            'post_type' => 'member_logos',
            'post_status' => 'publish'
        );
    $string = '';
    $query = new WP_Query( $args );
    echo '<section>';
    
    echo '<div class="member_logos">';
    while ($query->have_posts()) : $query->the_post();
        $meta = get_post_meta(get_the_ID(), '', false);
        echo '<div class="item logo_'.get_the_ID().'" id="logo_'.get_the_ID().'" >';
        echo '<a href="'.$meta['member_link'][0].'" title="'.get_the_title().'">';
        echo '<img src="';
        echo the_post_thumbnail_url('full');
        echo '" alt="'.get_the_title().'" />';
        echo '</a>';
        echo '</div>';
    endwhile;
    echo '</div>';
    echo '</section>';
    wp_reset_query();
}

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/*------------------------------------*\
	Customizer Functions
\*------------------------------------*/

function mytheme_customize_register( $wp_customize ) {
    $wp_customize->add_section( 'logo_info_section' , array(
        'title'      => __( 'Site Logo', 'logo' ),
        'priority'   => 1,
        'description' => 'Upload a logo to replace the default site name and description in the header',
    ) );
    $wp_customize->add_setting('logo_info');
    $wp_customize->add_setting('logo_fallback');
    $wp_customize->add_setting('logo_width', array(
       'default'    => '300px',
        'transport' => 'refresh',
    ));
    
    $wp_customize->add_control('logo_width', array(
	'label'        => __( 'Logo Width', 'mytheme' ),
	'section'    => 'logo_info_section',
	'settings'   => 'logo_width',
    'priority' => 3,
) ) ;
    $wp_customize->add_control( new WP_Customize_Upload_Control( $wp_customize, 'logo_info', 
	array(
		'label'      => __( 'Upload a Logo File (svg)', 'mytheme' ),
		'section'    => 'logo_info_section',
		'settings'   => 'logo_info',
        'priority' => 1,
	) ) 
);
    $wp_customize->add_control( new WP_Customize_Upload_Control( $wp_customize, 'logo_fallback', 
	array(
		'label'      => __( 'Upload a Fallback Logo File (png, jpg)', 'mytheme' ),
		'section'    => 'logo_info_section',
		'settings'   => 'logo_fallback',
        'priority' => 2,
	) ) 
);
}
add_action( 'customize_register', 'mytheme_customize_register' );
function set_logo_width() {
    ?>
    <style type="text/css">
        .header_logo{max-width: <?php echo get_theme_mod('logo_width', '300px')?>;}
        .logo {
            min-width: <?php echo get_theme_mod('logo_width', '300px')?>;
        }
    </style>
    <?php
}
add_action( 'wp_head', 'set_logo_width');

//Custom Settings Page

function custom_settings_add_menu() {
    add_menu_page('Custom Settings', 'Custom Settings', 'manage_options', 'custom-settings', 'custom_settings_page', null, 99);
}
add_action('admin_menu', 'custom_settings_add_menu');

function custom_settings_page() { ?>
    <div class="wrap">
        <h1>Custom Settings</h1>
        <form action="options.php" method="post">
            <?php
            settings_fields('section');
            do_settings_sections('theme-options');
            submit_button();
            ?>
        </form>
    </div>
<?php    
}

function setting_twitter() { 
            ?>
    <input type="text" name="twitter" id="twitter" value="<?php echo get_option('twitter'); ?>" />
<?php
}

function setting_favicon() {
    ?>
    <p>
        <input type="text" name="favicon[image]" id="favicon[image]" class="meta-image regular-text" value="<?php $the_image_url =  get_option('favicon'); echo $the_image_url['image']; ?>" />
        <input type="button" class="button image-upload" value="Browse" /> <br />
        <label>Current Image:</label><br /><br />
        <img src="<?php echo $the_image_url['image']; ?>" alt="Current Image" style="max-width: 200px; padding: 5px; border: 1px solid #bbbbbb" />
    </p>
    <script>
jQuery(document).ready(function($){
    $('.image-upload').click(function(e) {
        e.preventDefault();
        var image = wp.media({ 
            title: 'Upload Image',
            // mutiple: true if you want to upload multiple files at once
            multiple: false
        }).open()
        .on('select', function(e){
            // This will return the selected image from the Media Uploader, the result is an object
            var uploaded_image = image.state().get('selection').first();
            // We convert uploaded_image to a JSON object to make accessing it easier
            // Output to the console uploaded_image
            console.log(uploaded_image);
            var image_url = uploaded_image.toJSON().url;
            // Let's assign the url value to the input field
            $('.meta-image').val(image_url);
        });
    });
});
    </script>
    <?php
}
function load_wp_media_files() {
    wp_enqueue_media();
}
add_action( 'admin_enqueue_scripts', 'load_wp_media_files' );

function custom_settings_page_setup() {
    add_settings_section('section', 'All Settings', null, 'theme-options');
    add_settings_field('twitter', 'Twitter URL', 'setting_twitter', 'theme-options', 'section');
    register_setting('section', 'twitter');
    add_settings_field('favicon', 'Site Favicon', 'setting_favicon', 'theme-options', 'section');
    register_setting('section', 'favicon');
}

add_action('admin_init', 'custom_settings_page_setup');


                                 
/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

//Add post type Homepage Sliders
function create_post_homepage_sliders() {
    register_post_type('homepage_sliders', array(
        'labels'    =>  array(
            'name'  =>  __('Hero Sliders'),
            'singular_name' =>  __('Homepage Hero Slider'),
            'add_new'   =>  __('Add New Slider'),
            'edit_item' =>  __('Edit Slider'),
            'add_new_item'  =>  __('Add New Homepage Slider'),
            'search_items'  =>  __('Search Sliders'),
            'featured_image'    => __('Background Image'),
            'set_featured_image'    =>  __('Set Background Image'),
            'remove_featured_image' =>  __('Remove Background Image')
        ),
        'public'    => true,
        'show_ui'   => true,
        'has_archive' => false,
        'supports' => array('title', 'thumbnail'),
        'menu_icon' =>  'dashicons-images-alt',
        'menu_position' =>  5,
        'publicly_queryable'    => false,
        
    ));
}

//Add post type Member Logos
function create_post_member_logos() {
    register_post_type('member_logos', array(
        'labels'    =>  array(
            'name'  =>  __('Member Logos'),
            'singular_name' =>  __('Member Logo'),
            'add_new'   =>  __('Add New Logo'),
            'add_new_item'  =>  __('Add New Logo'),
            'featured_image'    => __('Member Logo'),
            'set_featured_image'    =>  __('Set Member Logo'),
            'remove_featured_image' =>  __('Remove Member Logo'),
            'edit_item' =>  __('Edit Logo'),
        ),
        'public'    =>  true,
        'show_ui'   =>  true,
        'has_archive'   =>  false,
        'supports'  =>  array('title', 'thumbnail'),
        'menu_icon' =>  'dashicons-groups',
        'menu_position' =>  6,
        'publicly_queryable'    => false,
    ));
}

//Add post type Featured Content Sliders
function create_post_featured_sliders() {
    register_post_type('featured_sliders', array(
        'labels'    =>  array(
            'name'  =>  __('Featured Content'),
            'singular_name' =>  __('Featured Content Slider'),
            'add_new'   =>  __('Add New Slider'),
            'add_new_item'  =>  __('Add New Slider'),
            'featured_image'    => __('Background Image'),
            'set_featured_image'    =>  __('Set Background Image'),
            'remove_featured_image' =>  __('Remove Background Image'),
            'edit_item' =>  __('Edit Slider'),
        ),
        'public'    =>  true,
        'show_ui'   =>  true,
        'has_archive'   =>  false,
        'supports'  =>  array('title', 'thumbnail', 'excerpt'),
        'menu_icon' =>  'dashicons-star-filled',
        'menu_position' =>  7,
        'publicly_queryable'    =>  false,
    ));
}

function create_post_product_sheet() {
    register_post_type('product_sheet', array(
        'labels'    =>  array(
            'name'  =>  __('Product Sheets'),
            'singular_name' =>  __('Product Sheet'),
            'add_new'   =>  __('Add New Product Sheet'),
            'add_new_item'  =>  __('Add New Product Sheet'),
            'edit_item' =>  __('Edit Product Sheet'),
        ),
        'public'    =>  true,
        'show_ui'   =>  true,
        'has_archive'   =>  true,
        'supports'  =>  array('title','tumbnail','editor'),
        'taxonomies' => array('category'),
        'publicly_queryable'    =>  true,
        'menu_icon' => 'dashicons-admin-page',
    ));
}

/*------------------------------------*\
	Custom Meta Boxes
\*------------------------------------*/

//Add all custom meta boxes to respective post types
function admin_init(){
    //slider meta boxes
    add_meta_box('slider_link_meta', 'Slider Link', 'slider_link', 'homepage_sliders', 'side', 'high');
    add_meta_box('slider_heading_meta', 'Heading and Subheading', 'slider_heading', 'homepage_sliders', 'normal', 'high');
    add_meta_box('slider_overlay_meta', 'Overlay Color', 'slider_overlay_color', 'homepage_sliders', 'side', 'low');
    add_meta_box('slider_logo_theme_meta', 'Logo Theme', 'slider_logo_theme', 'homepage_sliders', 'side', 'low');
    //member link meta boxes
    add_meta_box('member_link_meta', 'Member Logo', 'member_link', 'member_logos', 'normal', 'high');
    remove_meta_box('postexcerpt', 'featured_sliders','normal');
    add_meta_box('postexcerpt', __('Subheadline'), 'post_excerpt_meta_box', 'featured_sliders','normal','high');
    add_meta_box('featured_sliders_button_meta', 'Slider Button', 'featured_sliders_button', 'featured_sliders', 'side','low');
    add_meta_box('featured_sliders_editor_meta', 'Content', 'featured_sliders_editor', 'featured_sliders', 'normal', 'high');
}

//Add meta box Slider Link for Homepage Sliders
function slider_link() {
    global $post;
    $custom = get_post_custom($post->ID);
    $slider_link = $custom['slider_link'][0];
    ?>
    <label>Slider Link: (include http://)</label>
    <br /><br />
    <input style="width: 100%; max-width: 400px;" name="slider_link" value="<?php echo $slider_link; ?>" placeholder="e.g. http://example.com" />
    <?php
}

//Add meta box Slider Heading and Subheading for Homepage Sliders
function slider_heading() {
    global $post;
    $custom = get_post_custom($post->ID);
    $slider_heading = $custom['slider_heading'][0];
    $slider_subheading = $custom['slider_subheading'][0];
    ?>
    <style>
        .slider_subheading_box {
            width: 100%;
            display: block;
        }
    </style>
    <label>Slider Heading</label>
    <br /><br />
    <input style="width: 100%; max-width: 400px;" name="slider_heading" value="<?php echo $slider_heading; ?>" />
    <br /><br />
    <label>Slider Subheading</label>
    <br /><br />
    <textarea class="slider_subheading_box" cols="50" rows="5" name="slider_subheading"><?php echo $slider_subheading; ?></textarea>
    <?php
}

//Create Colorpicker
function wpse_80236_Colorpicker(){
	wp_enqueue_style( 'wp-color-picker');
	//
	wp_enqueue_script( 'wp-color-picker');
}

//Add meta box Slider Overlay Color for Homepage Sliders
function slider_overlay_color() {
    global $post;
    $custom = get_post_custom($post->ID);
    $slider_overlay = $custom['slider_overlay'][0];
    ?>
    <label>Use a hex code, don't inclue #</label><br /><br />
    <input name="slider_overlay" type="text" value="<?php if( isset ($slider_overlay)) echo $slider_overlay; ?>" class="meta-color" />
    <script>
(function( $ ) {
	// Add Color Picker to all inputs that have 'color-field' class
	$(function() {
	$('.meta-color').wpColorPicker();
	});
})( jQuery );
</script>
    <?php
}

//Add meta box Slider Logo Theme Color for Homepage Slider
//Controls if a dark or reversed logo is used on the slider
function slider_logo_theme($post) {
    wp_nonce_field('slider_logo_theme', 'slider_logo_theme_nonce');
    $value = get_post_meta($post->ID, 'my_key', true);
    ?>
    <p>Pick a color for the logo on the slider. Default is Light.</p>
    <label style="margin-right: 20px;"><input type="radio" name="slider_logo_theme" value="light" <?php checked($value, 'light'); ?>> Light</label><label><input type="radio" name="slider_logo_theme" value="dark"<?php checked($value, 'dark'); ?>> Dark</label>
    <?php
}

//Add meta box Member Website Link for Member Logos
function member_link() {
    global $post;
    $custom = get_post_custom($post->ID);
    $member_link = $custom['member_link'][0];
    ?>
    <label>Link to Member Website (<em>include http://</em>):</label>
    <br /><br />
    <input name="member_link" value="<?php echo $member_link ?>" />
    <?php
}

function featured_sliders_button() {
    global $post;
    $custom = get_post_custom($post->ID);
    $button_link = $custom['featured_sliders_button_link'][0];
    $button_text = $custom['featured_sliders_button_text'][0];
    $button_color = $custom['featured_sliders_button_color'][0];
    ?>
    <label>Button Link<br />(Inclued http://)</label><br />
    <input type="text" name="featured_sliders_button_link" value="<?php echo $button_link?>">
    <br /><br />
    <label>Button Text</label><br />
    <input type="text" name="featured_sliders_button_text" value="<?php echo $button_text?>">
    <br /><br />
    <label>Button Color<br/>(Use only the palates at the bottom)</label><br />
    <input type="text" name="featured_sliders_button_color" value="<?php if(isset($button_color))echo $button_color?>" class="meta-button-color">
    <script>
(function( $ ) {
	// Add Color Picker to all inputs that have 'color-field' class
	$(function() {
	$('.meta-button-color').wpColorPicker({
        palettes: ['#111111', '#222222', '#333333', '#444444','#555555','#666666', '#777777']
    });
	});
})( jQuery );
</script>
    
    <?php
}

function featured_sliders_editor() {
    ?>
    <p style="margin-bottom: -25px;">Enter the content you want to appear before the button. This will take HTML, but does not need outer wrapping paragraph tags.</p>
    <?php
    global $post;
    $custom = get_post_custom($post->ID);
    $feat_slider_editor = get_post_meta($_GET['post'], 'featured_sliders_editor', true);
    wp_editor(htmlspecialchars_decode($feat_slider_editor), 'feat_sliders_editor', $settings = array('wpautop'=>true, 'media_buttons' =>false, 'teeny'=>true, 'textarea_name'=>'feat_sliders_editor_content'));
    ?>
    <style>
        #mceu_3, #mceu_5, #mceu_6, #mceu_7, #mceu_8, #mceu_9, #mceu_10, #mceu_11, #mceu_12, #mceu_13, #mceu_14 {
            display: none;
        }
    </style>
    <?php
}

//Save all meta boxes and their content, calls them back again when post is loaded 
function save_details() {
    global $post;
    
    update_post_meta($post->ID, 'slider_link', $_POST['slider_link']);
    update_post_meta($post->ID, 'slider_heading', $_POST['slider_heading']);
    update_post_meta($post->ID, 'slider_subheading', $_POST['slider_subheading']);
    update_post_meta($post->ID, 'slider_overlay', $_POST['slider_overlay']);
    update_post_meta($post->ID, 'member_link', $_POST['member_link']);
    update_post_meta($post->ID, 'slider_logo_theme', $_POST['slider_logo_theme']);
    update_post_meta($post->ID, 'featured_sliders_button_link', $_POST['featured_sliders_button_link']);
    update_post_meta($post->ID, 'featured_sliders_button_text', $_POST['featured_sliders_button_text']);
    update_post_meta($post->ID, 'featured_sliders_button_color', $_POST['featured_sliders_button_color']);
    if (!empty($_POST['feat_sliders_editor_content']))
        {
        $datta=htmlspecialchars($_POST['feat_sliders_editor_content']);
        update_post_meta($post->ID, 'featured_sliders_editor', $datta );
        }
}

//Saves radio meta box for Slider Logo Theme Color for Homepage Slider
function save_radio_box($post_id) {
    if (!isset($_POST['slider_logo_theme_nonce'])) {
        return;
    }
    if (!wp_verify_nonce($_POST['slider_logo_theme_nonce'], 'slider_logo_theme')) {
        return;
    }
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    if (!current_user_can('edit_post', $post_id)){
        return;
    }
    $new_meta_value = (isset( $_POST['slider_logo_theme']) ? sanitize_html_class($_POST['slider_logo_theme']) :'');
    update_post_meta($post_id, 'my_key', $new_meta_value);
}

add_action('wp_ajax_nopriv_product_sheet_filter_search', 'product_sheet_filter_search');
add_action('wp_ajax_product_sheet_filter_search', 'product_sheet_filter_search');
add_action('wp_ajax_nopriv_product_sheet_get', 'product_sheet_get');
add_action('wp_ajax_product_sheet_get', 'product_sheet_get');

//AJAX FUNCTIONS

function product_sheet_filter_search() {
    $query = $_POST['query'];
    $args = array(
            'post_type' => 'product_sheet',
            'post_status' => 'publish',
            's' => $query
        );
    $string = '';
    $query = new WP_Query( $args );
    if ($query->have_posts()) {
        echo '<ul>';
        while ($query->have_posts()) : $query->the_post();
            echo '<li class="menu-item menu-item-type-post_type menu-item-object-product_sheet">';
            echo '<a class="search_result_page_link" href="'.get_permalink().'">';
            echo get_the_title();
            echo '</a></li>';
        endwhile;
        echo '</ul>';
    } else {
        echo 'Nothing found';
    }

    die();
}

function product_sheet_get() {
    $query = $_POST['query'];
    
    $query = rawurldecode($query);
    
    $parts  = explode('/', $query);
    $output = implode('/', array_slice($parts, 4, 5));
    
    $new_query = get_page_by_path($output, OBJECT, 'product_sheet');
    
    echo '<h2>'.$new_query->post_title.'</h2>';
    echo '<span class="product_sheet_last_update" id="product-sheet-last-update">Last updated '.mysql2date('M j Y', $new_query->post_date).'</span>';
    echo '<div class="product_content">'.$new_query->post_content.'</div>';
    ?>
        <div class="product_sheet_content_loader" id="product-sheet-content-loader" style="display:block; opacity: 1;">
            <div class="loader"></div>
        </div>
    <?php
    
    die();
}

?>